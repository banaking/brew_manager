import json

from django.http import JsonResponse, HttpResponse
from django.views.generic import View
from django.utils.decorators import method_decorator
import random


class Test(View):
    http_method_names = ['get']

    def get (self, *args, **kwargs):

        api_resp = {'success':True}

        return JsonResponse(api_resp, status=400)


class AdminAuth(View):
    http_method_names = ['post', 'get']

    def get (self, *args, **kwargs):

        auth_form = '''
        <!DOCTYPE html>
		<html>
		<body>

		<form action="/admin_auth" method="post">
		  login:<br>
		  <input type="text" name="login" value="">
		  <br>
		  password :<br>
		  <input type="text" name="password" value="">
		  <br><br>
		  <input type="submit" value="Отправить">
		</form>

		</body>
		</html>

        '''

        return HttpResponse(auth_form)


    def post (self, *args, **kwargs):

        login = self.request.POST.get('login')
        password = self.request.POST.get('password')

        actual_password = "42004212321"
        actual_login    = "mrhacker"

        some_string = ''

        if( login and password ):
            if (actual_login == login):
                if (actual_password == password):
                    some_string = 'Ты молодец, возьми с полки пирожок, секретный код 424242424242'
                elif(len(password) != len(actual_password)):
                    some_string = '14) У актуального пароля({}) и текущего разница в кол-ве {} знаков'.format( random.randint(56,99), len(actual_password) - len(password))
                else:
                    n = 0
                    for i in range(11):
                        if (password[i] == actual_password[i]):
                            n += 1
                        else:
                            break
                    some_string = '89) У пароля ({})совпадает перыве {} цифр(ы)'.format(random.randint(1,49),n)
            elif ( len(login) != len(actual_login) ):
                some_string = '14) У актуального логина({}) и текущего разница в кол-ве {} знаков'.format( random.randint(56,99), len(actual_login) - len(login))
            else:
                n = 0
                for i in range(8):
                    if (actual_login[i] == login[i]):
                        n += 1
                    else:
                        break
                some_string = '42) У логина совпадают перыве {} значения(ий)'.format(n)
        else:
            some_string = 'Чего то не хватает'




        response = '''
            <!DOCTYPE html>
            <div class="how bad boy?"</div>
            <html>
            <body>
            {}
            </body>
            </html>

        '''.format(some_string)

        return HttpResponse(response)


